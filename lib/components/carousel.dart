import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class Carousel extends StatefulWidget {
  const Carousel({Key key}) : super(key: key);

  @override
  _CarouselState createState() => _CarouselState();
}

class _CarouselState extends State<Carousel> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
          children: [
            CarouselSlider(
              items: [
                "assets/images/doraemon.jpeg",
                "assets/images/parasite.jpeg",
                "assets/images/angelhasfallen.jpeg",
                "assets/images/spiderman.jpeg",
                "assets/images/mib.jpeg",
                "assets/images/friend_zone.jpg",
                "assets/images/alladin.jpg",
                "assets/images/avenger.jpg",
                "assets/images/c_marvel.jpg",
              ].map((i) {
                return Builder(
                  builder: (BuildContext context) {
                    return Container(
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.symmetric(horizontal: 5.0),
                        decoration: BoxDecoration(
                            color: Colors.white
                        ),
                        child: Image.asset(i)
                    );
                  },
                );
              }).toList(),
              options: CarouselOptions(
                height: 300,
                initialPage: 0,
                enableInfiniteScroll: true,
                autoPlay: true,
                autoPlayInterval: Duration(seconds: 5),
              ),
            )
          ],
        )
    );
  }
}
