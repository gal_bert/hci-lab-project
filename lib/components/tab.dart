import 'package:bj_movies/Components/carousel.dart';
import 'package:bj_movies/screens/detail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class TabComponent extends StatefulWidget {
  const TabComponent({Key key}) : super(key: key);

  @override
  _TabComponentState createState() => _TabComponentState();
}

class _TabComponentState extends State<TabComponent>  with SingleTickerProviderStateMixin {

  TabController _tabController;

  @override
  void initState() {
    super.initState();
   _tabController = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: NestedScrollView(
          headerSliverBuilder: (context, value) {
            return [
              SliverAppBar(
                pinned: true,
                backgroundColor: Colors.white,
                flexibleSpace: FlexibleSpaceBar(
                  background: Container(
                      height: 200,
                      child: Carousel()
                  ),

                ),
                expandedHeight: 350.0,
                floating: true,
                bottom:
                TabBar(
                  labelColor: Colors.blue,
                  unselectedLabelColor: Colors.black,
                  controller: _tabController,
                  tabs: <Widget>[
                    Tab(child: Text("New Movie")),
                    Tab(child: Text("Popular Movie"),)
                  ],
                ),
              ),
            ];
          },
          body: TabBarView(
            controller: _tabController,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [

                  Container(
                    margin: EdgeInsets.fromLTRB(10, 20, 10, 0),
                    child: ListTile(
                      title: Text("Aladin"),
                      subtitle: Text("Genre: Drama"),
                      leading: Image.asset("assets/images/alladin.jpg"),
                      onTap: () {
                        // Navigator.pushNamed(context, Detail.routeName);

                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Detail(
                                img: "assets/images/alladin.jpg",
                                title: "Alladin",
                                synopsis: "When street rat Aladdin frees a genie from a lamp, he finds his wishes granted. However, he soon finds that the evil has other plans for the lamp -- and for Princess Jasmine. But can Aladdin save Princess Jasmine and his love for her after she sees that he isn't quite what he appears to be?",
                                genre: "Drama"
                            ),
                        ));

                      },
                    ),
                    decoration: BoxDecoration(
                      border: Border(
                        top: BorderSide(color: Colors.grey),
                        bottom: BorderSide(color: Colors.grey),
                      )
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: ListTile(
                      title: Text("Avengers"),
                      subtitle: Text("Genre: Action"),
                      leading: Image.asset("assets/images/avenger.jpg"),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Detail(
                                  img: "assets/images/avenger.jpg",
                                  title: "Avenger",
                                  synopsis: "When Thor's evil brother, Loki (Tom Hiddleston), gains access to the unlimited power of the energy cube called the Tesseract, Nick Fury (Samuel L. Jackson), director of S.H.I.E.L.D., initiates a superhero recruitment effort to defeat the unprecedented threat to Earth.",
                                  genre: "Action"
                              ),
                            ));
                      },
                    ),
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(color: Colors.grey),
                      )
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: ListTile(
                      title: Text("Captain Marvel"),
                      subtitle: Text("Genre: Action"),
                      leading: Image.asset("assets/images/c_marvel.jpg"),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Detail(
                                  img: "assets/images/c_marvel.jpg",
                                  title: "Captain Marvel",
                                  synopsis: "Captain Marvel is an extraterrestrial Kree warrior who finds herself caught in the middle of an intergalactic battle between her people and the Skrulls. Living on Earth in 1995, she keeps having recurring memories of another life as U.S.",
                                  genre: "Action"
                              ),
                            ));
                      },
                    ),
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(color: Colors.grey),
                      )
                    ),
                  ),

                ],
              ),

              SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 20, 10, 0),
                      child: ListTile(
                        title: Text("Doraemon"),
                        subtitle: Text("Genre: Drama"),
                        leading: Image.asset("assets/images/doraemon.jpeg"),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => Detail(
                                    img: "assets/images/doraemon.jpeg",
                                    title: "Doraemon",
                                    synopsis: "Doraemon, a cat robot from the 22nd century, is sent to help Nobita Nobi, a young boy who receives poor grades and is frequently bullied by his two classmates, Takeshi Goda (nicknamed Gian) and Suneo Honekawa (Gian's sidekick).",
                                    genre: "Drama"
                                ),
                              ));
                        },
                      ),
                      decoration: BoxDecoration(
                          border: Border(
                            top: BorderSide(color: Colors.grey),
                            bottom: BorderSide(color: Colors.grey),
                          )
                      ),
                    ),

                    Container(
                      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: ListTile(
                        title: Text("Men In Black"),
                        subtitle: Text("Genre: Action"),
                        leading: Image.asset("assets/images/mib.jpeg"),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => Detail(
                                    img: "assets/images/mib.jpeg",
                                    title: "Men In Black",
                                    synopsis: "They are the best-kept secret in the universe. Working for a highly funded yet unofficial government agency, Kay (Tommy Lee Jones) and Jay (Will Smith) are the Men in Black, providers of immigration services and regulators of all things alien on Earth.",
                                    genre: "Action"
                                ),
                              ));
                        },
                      ),
                      decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(color: Colors.grey),
                          )
                      ),
                    ),

                    Container(
                      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: ListTile(
                        title: Text("Parasite"),
                        subtitle: Text("Genre: Thriller"),
                        leading: Image.asset("assets/images/parasite.jpeg"),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => Detail(
                                    img: "assets/images/parasite.jpeg",
                                    title: "Parasite",
                                    synopsis: "The Kim family lives in a semi-basement and struggles to keep food on the table. They take on odd jobs for cash like folding pizza boxes, and they rely on unprotected wi-fi networks and street-cleaning pesticides to keep their home insect-free.",
                                    genre: "Action"
                                ),
                              ));
                        },
                      ),
                      decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(color: Colors.grey),
                          )
                      ),
                    ),

                    Container(
                      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: ListTile(
                        title: Text("Angel Has Fallen"),
                        subtitle: Text("Genre: Action"),
                        leading: Image.asset("assets/images/angelhasfallen.jpeg"),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => Detail(
                                    img: "assets/images/angelhasfallen.jpeg",
                                    title: "Angel Has Fallen",
                                    synopsis: "Authorities take Secret Service agent Mike Banning into custody for the failed assassination attempt of U.S. President Allan Trumbull. After escaping from his captors, Banning must evade the FBI and his own agency to find the real threat to the president.",
                                    genre: "Action"
                                ),
                              ));
                        },
                      ),
                      decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(color: Colors.grey),
                          )
                      ),
                    ),

                    Container(
                      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: ListTile(
                        title: Text("Spiderman"),
                        subtitle: Text("Genre: Action"),
                        leading: Image.asset("assets/images/spiderman.jpeg"),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => Detail(
                                    img: "assets/images/spiderman.jpeg",
                                    title: "Spiderman",
                                    synopsis: "Spider-Man centers on student Peter Parker (Tobey Maguire) who, after being bitten by a genetically-altered spider, gains superhuman strength and the spider-like ability to cling to any surface. He vows to use his abilities to fight crime, coming to understand the words of his beloved Uncle Ben",
                                    genre: "Action"
                                ),
                              ));
                        },
                      ),
                      decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(color: Colors.grey),
                          )
                      ),
                    ),

                    Container(
                      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: ListTile(
                        title: Text("Friend Zone"),
                        subtitle: Text("Genre: Drama"),
                        leading: Image.asset("assets/images/friend_zone.jpg"),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => Detail(
                                    img: "assets/images/friend_zone.jpg",
                                    title: "Friend Zone",
                                    synopsis: "In this world, there are many people who seem to be wandering along a relationship on the border of friends and lovers. This borderline is also commonly known as the friend zone and then two friends start to have romantic feelings for each other",
                                    genre: "Drama"
                                ),
                              ));
                        },
                      ),
                      decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(color: Colors.grey),
                          )
                      ),
                    ),

                  ],
                ),
              ),

            ],
          ),
        )
    );
  }
}
