import 'package:bj_movies/Screens/about.dart';
import 'package:bj_movies/screens/detail.dart';
import 'package:flutter/material.dart';

import 'Screens/home.dart';
import 'Screens/login.dart';

final Map<String, WidgetBuilder> routes = {

  Login.routeName: (context) => Login(),
  Home.routeName: (context) => Home(),
  Detail.routeName: (context) => Detail(),
  About.routeName: (context) => About(),

};
