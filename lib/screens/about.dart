import 'package:flutter/material.dart';

class About extends StatelessWidget {
  static String routeName = "/about";

  final String title = "About BJ Movies";
  final String description = "BJ Movies is a movie catalogue application that serves the latest and most popular movie in the industry. This application is totally independent with no affiliations or any other forms of cooperation with movie related companies to maintain a neutral credibility of our content to the viewers.";
  final String imgPath = "assets/images/logo.png";

  const About({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(
            height: 42,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              title,
              style: TextStyle(fontSize: 24),
              textAlign: TextAlign.center,
            ),
          ),

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.asset(
              imgPath,
              width: 200,
              height: 200,
            ),
          ),

          Padding(
            padding: const EdgeInsets.fromLTRB(32, 10, 32, 10),
            child: Text(description),
          )
        ],
      ),
    );
  }
}
