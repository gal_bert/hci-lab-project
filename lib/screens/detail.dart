import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Detail extends StatefulWidget {
  static String routeName = "/detail";

  String title, genre, synopsis, img;
  Detail({
    Key key,
    @required this.img,
    @required this.title,
    @required this.genre,
    @required this.synopsis,
  }) : super(key: key);

  @override
  _DetailState createState() => _DetailState();
}


class _DetailState extends State<Detail> {

  TextEditingController _reviewController = TextEditingController();

  final String alertTitle = "Review is too long!";
  final String alertContent = "Your review must be less than 1000 characters";

  void validate(BuildContext context){
    String review = _reviewController.text.toString();
    if(review.length > 1000){
      showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: Text(alertTitle),
          content: Text(alertContent),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, 'OK'),
              child: const Text('Okay'),
            ),
          ],
        ),
      );
    } else {
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: [
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                child: Image.asset(
                  widget.img,
                  height: 150,
                ),
              ),

              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    widget.title,
                    style: TextStyle(fontSize: 24),
                  ),
                  Text(""),
                  Text(
                    "Genre: " + widget.genre,
                    style: TextStyle(fontSize: 16),
                  ),
                ],
              ),
            ],
          ),

          Padding(
            padding: const EdgeInsets.all(32.0),
            child: Container(
              child: Text(
                widget.synopsis,
                style: TextStyle(fontSize: 16),
              ),
            ),
          ),

          Padding(
            padding: const EdgeInsets.fromLTRB(32,12,32,12),
            child: TextField(
              decoration: InputDecoration(
                  hintText: "Input review",
                  border: OutlineInputBorder()
              ),
              controller: _reviewController,
            ),
          ),

          Padding(
              padding: const EdgeInsets.fromLTRB(100,24,100,12),
              child: SizedBox(
                width: 100,
                height: 40,
                child: ElevatedButton(
                    style: ButtonStyle(
                      foregroundColor:
                      MaterialStateProperty.all<Color>(Colors.white),
                      overlayColor: MaterialStateProperty.resolveWith<Color>(
                            (Set<MaterialState> states) {
                          if (states.contains(MaterialState.focused) ||
                              states.contains(MaterialState.pressed))
                            return Colors.deepPurpleAccent;
                          return null; // Defer to the widget's default.
                        },
                      ),
                    ),
                    onPressed:() {
                      validate(context);
                    },
                    child: Text("Submit")
                ),
              )
          ),

        ],
      ),
    );
  }
}
