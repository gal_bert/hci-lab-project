import 'package:bj_movies/components/tab.dart';
import 'package:flutter/material.dart';

import 'about.dart';
import 'login.dart';

class Home extends StatefulWidget {
  static String routeName = "/home";

  final String username;
  Home({Key key, @required this.username}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("BJ Movies"),
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(16, 24, 16, 24),
              child: SizedBox(
                child: Text(
                  "Hello, " + widget.username,
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),

            ListTile(
              title: Text('Home'),
              onTap: (){

              },
            ),

            ListTile(
              title: Text('About Us'),
              onTap: (){
                Navigator.pushNamed(context, About.routeName);
              },
            ),

            ListTile(
              title: Text('Logout'),
              onTap: (){
                Navigator.pushReplacementNamed(context, Login.routeName);
              },
            ),

          ],
        ),
      ),
      body: TabComponent(),
    );
  }
}
