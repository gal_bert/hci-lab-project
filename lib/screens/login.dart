import 'package:bj_movies/Screens/home.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  static String routeName = "/login";

  const Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  String username, password, errorMessage;
  String putMessage = " ";
  TextEditingController _usernameController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  void validate(BuildContext context){
    username = _usernameController.text.toString();
    password = _passwordController.text.toString();

    if(username.isEmpty || password.isEmpty){
      errorMessage = "All fields must be filled!";
    }
    else if(username.contains(" ")){
      errorMessage = "Username can't contain space!";
    }
    else if(username.length < 5 || username.length > 20){
      errorMessage = "Username must be 5-20 characters!";
    }
    else {
      errorMessage = " ";
      Navigator.pop(context);
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => Home(username: username),
          ));
    }

    setState(() {
      putMessage = errorMessage;
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [

          Padding(
            padding: const EdgeInsets.fromLTRB(0,0,0,16),
            child: Text(
              "Welcome Back!",
              style: TextStyle(fontSize: 28),
              textAlign: TextAlign.center,
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0,0,0,16),
            child: Text(
              "Please login to get started",
              style: TextStyle(fontSize: 16),
              textAlign: TextAlign.center,
            ),
          ),

          SizedBox(
            height: 48,
          ),

          Padding(
            padding: const EdgeInsets.fromLTRB(48,12,48,12),
            child: TextField(
              decoration: InputDecoration(
                  hintText: "Username",
                  border: OutlineInputBorder()
              ),
              controller: _usernameController,
            ),
          ),

          Padding(
            padding: const EdgeInsets.fromLTRB(48,12,48,12),
            child: TextField(
              decoration: InputDecoration(
                  hintText: "Password",
                  border: OutlineInputBorder()
              ),
              controller: _passwordController,
              obscureText: true,
            ),
          ),

          Padding(
            padding: const EdgeInsets.fromLTRB(8,28,8,8),
            child: Text(
              '$putMessage',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.red),
            ),
          ),

          Padding(
            padding: const EdgeInsets.fromLTRB(140,24,140,12),
            child: SizedBox(
              width: 70,
              height: 40,
              child: ElevatedButton(
                  style: ButtonStyle(
                      foregroundColor:
                          MaterialStateProperty.all<Color>(Colors.white),
                      overlayColor: MaterialStateProperty.resolveWith<Color>(
                        (Set<MaterialState> states) {
                          if (states.contains(MaterialState.focused) ||
                              states.contains(MaterialState.pressed))
                            return Colors.deepPurpleAccent;
                          return null; // Defer to the widget's default.
                        },
                      ),
                    ),
                    onPressed:() {
                    validate(context);
                    // debugPrint('MYLOG Username: $username');
                    // debugPrint('MYLOG Password: $password');
                    // debugPrint('MYLOG Error: $errorMessage');
                  },
                  child: Text("Login")
              ),
            )
          ),



        ],
      ),
    );
  }
}
